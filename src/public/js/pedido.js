const carro = new Carrito();
const productos = document.getElementById('lista-productos');
const cantidad_productos=document.getElementById('cant_producto')

cargarEventos();

function cargarEventos(){
    productos.addEventListener('click', (e) => { carro.comprarproducto(e) });
    document.addEventListener('DOMContentLoaded',carro.leerLocalStorage());
}
