class Carrito {
    //añadir producto al carrito
    
    comprarproducto(e) {
        e.preventDefault();
        if (e.target.classList.contains('agregar-carrito')) {
            const producto = e.target.parentElement;
            this.leerDatosProducto(producto);
        }
    }
    comprarproductosp(e) {
        e.preventDefault();
        if (e.target.classList.contains('agregar-carrito')) {
            const producto = e.target.parentElement.parentElement;
            this.leerDatosProductosp(producto);
        }
    }

    leerDatosProducto(producto) {
        const infoProducto = {
            imagen: producto.querySelector('img').src,
            descripcion: producto.querySelector('h5').textContent,
            precio: producto.querySelector('span').textContent,
            id: producto.querySelector('a').getAttribute('data-id'),
            cantidad: 1
        }
        let productosLS;
        productosLS = this.obtenerProductosLocalStorage();
        productosLS.forEach(function (productoLS) {
            if (productoLS.id === infoProducto.id) {
                productosLS = productoLS.id;
            }
        })
        if (productosLS === infoProducto.id) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'El producto ya se encuentra en el carrito!',
            })
        }
        else {
            this.insertarCarrito(infoProducto)
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Agregado correctamente',
                showConfirmButton: false,
                timer: 1000
            });
        }

    }

    leerDatosProductosp(producto) {
        const infoProducto = {
            imagen: producto.querySelector('img').src,
            descripcion: producto.querySelector('h5').textContent,
            detalle: producto.querySelector('p').textContent,
            precio: producto.querySelector('span').textContent,
            id: producto.querySelector('a').getAttribute('data-id'),
            cantidad: Math.abs(cant_p.value)
        };
        let productosLS;
        productosLS = this.obtenerProductosLocalStorage();
        productosLS.forEach(function (productoLS) {
            if (productoLS.id === infoProducto.id) {
                productosLS = productoLS.id;
            }
        })
        if (productosLS === infoProducto.id) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'El producto ya se encuentra en el carrito!',
            })
        }
        else {
            this.insertarCarrito(infoProducto)
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Agregado correctamente',
                showConfirmButton: false,
                timer: 1000
            });
        }

    }

    insertarCarrito(producto) {
        this.guardarProductosLocalStorage(producto);
        this.leerLocalStorage();
    }

    vaciarCarrito(e){
        e.preventDefault();
        if (e.target.classList.contains('vaciar-carrito')) {
            this.vaciarLocalStorage();
        }
        
    }

    eliminarProducto(e) {
        e.preventDefault();
        let producto, productoID;
        if (e.target.classList.contains('borrar-producto')) {
            e.target.parentElement.parentElement.parentElement.remove();
            producto = e.target.parentElement.parentElement.parentElement;
            productoID = producto.querySelector('a').getAttribute('data-id');
            this.eliminarProductoLocalStorage(productoID);
            this.calcularTotal();
        }
    }
    
    guardarProductosLocalStorage(producto) {
        let productos;
        productos = this.obtenerProductosLocalStorage();
        productos.push(producto);
        let now = Date.now();
        let expira = now + (3 * 60 * 60 * 1000);
        localStorage.setItem('hora', expira);
        localStorage.setItem('productos', JSON.stringify(productos));
    }

    obtenerProductosLocalStorage() {
        let productoLS;
        if (localStorage.getItem('productos') === null) {
            productoLS = [];
        }
        else {
            productoLS = JSON.parse(localStorage.getItem('productos'));
        }
        return productoLS;
    }
    eliminarProductoLocalStorage(productoID) {
        let productosLS;
        productosLS = this.obtenerProductosLocalStorage();
        productosLS.forEach(function (productoLS, index) {
            if (productoLS.id === productoID) {
                productosLS.splice(index, 1);
            }
        });
        if(productosLS.length==0){
            this.vaciarLocalStorage();
        }
        else{
            localStorage.setItem('productos', JSON.stringify(productosLS));
            this.leerLocalStorage();
        }
    }
    leerLocalStorage() {
        let expiracion = localStorage.getItem('hora');
        if (expiracion != null) {
            let ahora = Date.now();
            if (parseInt(expiracion) < ahora) {
                this.vaciarLocalStorage();
            }
        }
        let productosLS;
        productosLS = this.obtenerProductosLocalStorage();
        cantidad_productos.innerHTML = "";
        cantidad_productos.innerHTML += productosLS.length;
    }

    vaciarLocalStorage() {
        localStorage.clear();
        location = "carrito";
    }

    leerLocalStorageCompra() {
        let expiracion = localStorage.getItem('hora');
        if (expiracion != null) {
            let ahora = Date.now();
            if (parseInt(expiracion) < ahora) {
                this.vaciarLocalStorage();
            }
        }
        let productosLS;
        productosLS = this.obtenerProductosLocalStorage();
        cantidad_productos.innerHTML = "";
        cantidad_productos.innerHTML += productosLS.length;
        if (productosLS.length == 0) {
            const row = document.createElement('div');
            row.innerHTML = `
            <h2 class="centrar">NO HAY PRODUCTOS EN EL CARRITO</h2>
            `;
            listaCompra.appendChild(row);
        }
        else {
            const row = document.createElement('div');
            row.innerHTML = `
                <div class="contenedor-producto bg-gris blanco carrito-encabezado">
                    <div class="box">Imagen</div>
                    <div class="box detalle-p">Descripcion del producto</div>
                    <div class="box">Cantidad</div>
                    <div class="box">Precio</div>
                    <div class="box">Total</div>
                </div>
                <div class="bg-gris blanco carrito-encabezado2">
                    <div class="box">Detalles de la compra</div>
                </div>
            `;
            listaCompra.appendChild(row);
            let total_carrito = 0;
            productosLS.forEach(function (producto) {
                let cant_pro=producto.cantidad
                if(cant_pro<1){
                    cant_pro=1;
                }
                const row = document.createElement('div');
                row.innerHTML = `
                    <div class="contenedor-producto bg-blanco">
                        <div class="box">
                            <img src="${producto.imagen}" height=50>
                        </div>
                        <div class="box detalle-p">${producto.descripcion}</div>
                        <div class="box">
                            <input type="number" class="pequeño cantidad" min="1" value=${cant_pro} required>
                            <a href="#" class="a-eliminar no-margin borrar-producto" title="Eliminar" data-id=${producto.id}><i class="fas fa-trash-alt borrar-producto"></i></a>
                        </div>
                        <div class="box">$${producto.precio}</div>
                        <div class="box" id="subtotales">$${(producto.precio * cant_pro).toFixed(2)}</div>
                    </div>
                `;
                total_carrito += (producto.precio * cant_pro);
                listaCompra.appendChild(row);
            });
            const row_total = document.createElement('div');
            row_total.innerHTML = `
            <div class="contenedor-carrito">
                <div class="contenedor-total bg-blanco">
                    <div class="total">TOTAL POR ESTE CARRITO:</div>
                    <div class="box" id="total-carrito">$${total_carrito.toFixed(2)}</div>
                </div>
            </div>
            `;
            listaCompra.appendChild(row_total);

            const row_acciones = document.createElement('div');
            row_acciones.innerHTML = `
            <div class="acciones">
                <a href=""  class="boton boton-rojoc vaciar-carrito">Vaciar el carrito</a>
                <a onclick="location='comprar'" class="boton boton-verde finalizar_co">Completar compra</a>
            </div>
            `;
            listaCompra.appendChild(row_acciones);
        };
    } 
    calcularTotal() {
        let productosLS;
        let total = 0;
        productosLS = this.obtenerProductosLocalStorage();
        for (let i = 0; i < productosLS.length; i++) {
            let element = Number(productosLS[i].precio * productosLS[i].cantidad);
            total = total + element;
        }
        document.getElementById('total-carrito').innerText = "$" + total.toFixed(2);
    }
    obtenerEvento(e) {
        e.preventDefault();
        let id, cantidad, producto, productosLS;
        if (e.target.classList.contains('cantidad')) {
            producto = e.target.parentElement;
            id = producto.querySelector('a').getAttribute('data-id');
            cantidad = producto.querySelector('input').value;
            if(cantidad<1){
                cantidad=1;
                producto.querySelector('input').value=cantidad;
            }
            let actualizarMontos = document.querySelectorAll('#subtotales');
            productosLS = this.obtenerProductosLocalStorage();
            productosLS.forEach(function (productoLS, index) {
                if (productoLS.id === id) {
                    productoLS.cantidad = Math.abs(cantidad);
                    actualizarMontos[index].innerHTML ="$"+Number(Math.abs(cantidad)* productosLS[index].precio);
                }
            });
            localStorage.setItem('productos', JSON.stringify(productosLS));
            this.calcularTotal();
        }
        else {
            console.log("click afuera");
        }
    }
}