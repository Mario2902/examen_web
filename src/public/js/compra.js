const compra = new Carrito();
const listaCompra=document.querySelector('#lista-compra');
const cantidad_productos=document.getElementById('cant_producto')
const carrito = document.getElementById('lista-compra');

cargarEventos();

function cargarEventos(){
    document.addEventListener('DOMContentLoaded',compra.leerLocalStorageCompra());
    carrito.addEventListener('click',(e)=>{compra.eliminarProducto(e)});
    carrito.addEventListener('click',(e)=>{compra.vaciarCarrito(e)});
    carrito.addEventListener('change', (e) => { compra.obtenerEvento(e) });
    carrito.addEventListener('keyup', (e) => { compra.obtenerEvento(e) });
}